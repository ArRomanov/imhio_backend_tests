import pytest
from _pytest.config.argparsing import Parser
from _pytest.fixtures import FixtureRequest
from typing import Any, Dict


def pytest_addoption(parser: Parser) -> None:
    """Обрабатывает параметры, переданные в командной строке при запуске.

    Возможные параметры:
    url - URL тестовой площадки для аутентифицированного входа
    db - реквизиты подключения к БД

    :param parser: объект для добавления параметров командной строки
    """
    parser.addoption("--url", default="http://127.0.0.1:58001")
    parser.addoption("--db", default="dbname='core' user='postgres' host='127.0.0.1' port='5433' password='devpass'")


@pytest.fixture(scope="session")
def config(request: FixtureRequest) -> Dict[str, Any]:
    """Конфигурация для текущего запуска тестов.

    :param request: объект глобальной фикстуры для запуска тестов
    :return: словарь с конфигурацией для тестов
    """
    return {
        "url": request.config.getoption("--url"),
        "db": request.config.getoption("--db"),
    }
