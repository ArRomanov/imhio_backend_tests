import pytest
import requests
import psycopg2
from typing import Any, Dict

nps_endpoint = 'nps'


@pytest.mark.parametrize("user_action, feedback", [('0', ''), ('10', '  '), ('5', 'message')])
def test_nps_requests_positive(config: Dict[str, Any], user_action: str, feedback: str) -> None:
    """Позитивные проверки метода /nps.

    :param config: фикстура с параметрами для тестов
    :param user_action: оценка пользователя
    :param feedback: комментарий пользователя
    """
    body = {"user_action": user_action, "feedback": feedback}
    response = requests.post("{url}/{endpoint}".format(url=config['url'], endpoint=nps_endpoint), json=body)
    assert response.json()['status'] == 'ok'


@pytest.mark.parametrize("user_action, feedback",
                         [('-1', 'message_1'), ('11', 'message_1'), ('message_2', 'message_2'), (' ', 'message_3')])
def test_nps_requests_negative(config: Dict[str, Any], user_action: str, feedback: str) -> None:
    """Негативные проверки метода /nps.

    :param config: фикстура с параметрами для тестов
    :param user_action: оценка пользователя
    :param feedback: комментарий пользователя
    """
    body = {"user_action": user_action, "feedback": feedback}
    response = requests.post("{url}/{endpoint}".format(url=config['url'], endpoint=nps_endpoint), json=body)
    assert response.json()['status'] == 'error'
    assert 'errors' in response.json(), "Отсутствует массив errors"


def test_nps_required_parameters(config: Dict[str, Any]) -> None:
    """Проверяет валидацию обязательного параметра метода /nps.

    :param config: фикстура с параметрами для тестов
    """
    body = {"feedback": "message from user"}
    response = requests.post("{url}/{endpoint}".format(url=config['url'], endpoint=nps_endpoint), json=body)
    assert response.json()['status'] == 'error'
    assert 'errors' in response.json(), "Отсутствует массив errors"


def test_nps_not_required_parameters(config: Dict[str, Any]) -> None:
    """Проверяет необязательного параметра метода /nps.

    :param config: фикстура с параметрами для тестов
    """
    body = {"user_action": "5"}
    response = requests.post("{url}/{endpoint}".format(url=config['url'], endpoint=nps_endpoint), json=body)
    assert response.json()['status'] == 'ok'


mobile_user_agent = 'Mozilla/5.0 (Linux; Android 7.0; SM-G930VC Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/58.0.3029.83 Mobile Safari/537.36'
desktop_user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246'


def get_device_type_from_db(db_connecton_str: str) -> str:
    """Получает значение поля device из последней записи БД.

    :param db_connecton_str: строка подключения к БД
    :return: значение поля device
    """
    with psycopg2.connect(db_connecton_str) as conn:
        with conn.cursor() as cursor:
            cursor.execute(
                """
                SELECT
                    device
                FROM
                    t_feedback_models
                ORDER BY id DESC
                LIMIT 1;
                """
            )
            current, = cursor.fetchone()
            return current


@pytest.mark.parametrize("user_agent, result", [(mobile_user_agent, 'mobile'), (desktop_user_agent, 'desktop')])
def test_nps_define_device(config: Dict[str, Any], user_agent: str, result: str) -> None:
    """Проверяет, верно ли определяется тип устройства для метода /nps.

    :param config: фикстура с параметрами для тестов
    :param user_agent: строка для параметра User-Agent для заголовка
    :param result: результат определения User-Agent (значение поля device)
    """
    body = {"user_action": "5"}
    headers = {"User-Agent": user_agent}
    requests.post("{url}/{endpoint}".format(url=config['url'], endpoint=nps_endpoint), json=body, headers=headers)
    device_type = get_device_type_from_db(config['db'])
    assert device_type == result
